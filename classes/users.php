<?php
require_once 'db.php';
class Users extends Db
{
	function __construct()
	{
		parent:: __construct();
	}
	
	private function generateCode($length=8)
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPRQSTUVWXYZ0123456789";
		$code = "";
		$clen = strlen($chars) - 1;  
		while (strlen($code) < $length) {
				$code .= $chars[mt_rand(0,$clen)];  
		}
		return $code;
	}
	
	function users_reg($login, $password)
	{
		$type="us";
		$password=SHA1($password);
		$hash=$this->generateCode();
        $this->do_query("INSERT INTO users VALUES ('','$login','$password','$type','$hash')");
        $_SESSION['user_login'] = $login;
		$_SESSION['user_hash'] = $hash;
        $_SESSION['user_type'] = $type;
        header("Location: index.php");
	}
	
	function users_enter($login, $password)
	{
		$password=SHA1($password);
		$query=$this->do_query("SELECT * FROM users WHERE login = '$login'");
		$user_date = mysql_fetch_array($query);
		$hash=$this->generateCode();

		if ($user_date['password'] == $password)
		{
			$_SESSION['user_login'] = $user_date['login'];
			$_SESSION['user_hash'] = $hash;
			$_SESSION['user_type']  = $user_date['type'];
			$this->do_query("UPDATE users SET hash='$hash' WHERE login='$login'");
			if($user_date['type']=="us")
			{
				header("Location: index.php");

			}else if($user_date['type']=="ad"){
				header("Location: index.php?page=admin");
			}
			exit;
		}else{
			echo "Wrong password or login!";
		}
	}
	
	function is_admin($hash,$login,$type)
	{
		$q=$this->do_query("SELECT * FROM users WHERE hash='$hash'");
		$row = mysql_fetch_row($q);
		if($row[1]==$login && $row[3]==$type && $type=="ad")
			return true;
		else
			return false;
	}
	
	function is_user($hash,$login,$type)
	{
		$q=$this->do_query("SELECT * FROM users WHERE hash='$hash'");
		$row = mysql_fetch_row($q);
		if($row[1]==$login && $row[3]==$type)
			return true;
		else
			return false;
	}
	
	function users_exit()
	{
		unset($_SESSION['user_login']);
		unset($_SESSION['user_hash']);
		unset($_SESSION['user_type']);
		header("Location:index.php");	
	}
	
	
	function GetCartCount($id)
	{
		$id="c".$id;
		$s=$this->do_query("SHOW TABLES LIKE '".$id."'");
		if(mysql_num_rows($s)==1)
		{
			$q = $this->do_query("SELECT * FROM $id");
			return mysql_num_rows($q);
		}else{
			return 0;
		}
	}
	
	function AddBook($login,$isbn)
	{
		$s=$this->GetCartCount($login);
		$login="c".$login;
		if($s==0){
			$query="CREATE TABLE $login(id INT(20) AUTO_INCREMENT PRIMARY KEY, isbn VARCHAR(20))";
			$this->do_query($query);
		}
		 $q=$this->do_query("INSERT INTO $login SET isbn='$isbn'")or die(mysql_error());
		if($q)$s++;
		return $s;
	}
	
	function GetCart($login){
		$login="c".$login;
		$query="SELECT * FROM $login INNER JOIN books ON $login.isbn=books.isbn";
		return $this->do_query($query);
	}
	
	function RemoveFromCart($login,$id){
		$count=$this->GetCartCount($login);
		$login="c".$login;
		$this->do_query("DELETE FROM $login WHERE id='$id'");
		if($count<=1){
			$this->do_query("DROP TABLE $login");
		}
		return $count;
	}
	
	function IsCartExist($login){
		$login="c".$login;
		$result = $this->do_query("SHOW TABLES LIKE '$login'");
		return mysql_num_rows($result);
	}
	
	
}
?>