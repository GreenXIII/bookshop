$( document ).ready(function() {
	
	$.ajax({
		type: "POST",
		data: {datain:  $('#user').text()},
		url: "ajax/cart_count.php",
		success: function(data){
			$('.cart_count').text(data);
		}
	});
	
	$(".buy").click(function(){
		var isbn = $(this).parent().find(".isbn").text();
		$.ajax({
			type: "POST",
			data: {
					datain:  isbn,
					login:	$('#user').text()
				  
				  },
			url: "ajax/cart_add.php",
			success: function(data){
				$('.cart_count').text(data);
			}
		});
	});
	
	
	$(".remove").click(function(){
		$.ajax({
			type: "POST",
			data: {
					id:  $(this).parent().parent().find(".id").text(),
					login:	$('#user').text()
				  
				  },
			url: "ajax/rem_cart.php",
			success: function(data){
				//alert(data);
				if(data)window.location.reload();
				//$('.cart_count').text(data);
			}
		});
	});
	
});