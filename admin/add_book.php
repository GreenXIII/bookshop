<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" type=text/css href="../s.css">
</head>
<body>
<?php    
    if (isset($_POST['submit'])){
        $isbn = strip_tags(trim($_POST['isbn']));
		$author = strip_tags(trim($_POST['author']));
		$title = strip_tags(trim($_POST['title']));
		$genre = strip_tags(trim($_POST['genre']));
		$year = strip_tags(trim($_POST['year']));
		$desc = strip_tags(trim($_POST['desc']));
		$book->book_add($isbn, $author,$title,$genre,$year,$desc);
    }
	echo "<div class='head'><div class='right_head'>";
	require_once 'exit.php';
	echo "</div></div>";
?>
	<div id='twenty'></div>
	<div id='twenty'></div>
	<div id='twenty'></div>
	<div class="defaultform">
		<form method="post" action="">
			<input name="isbn" placeholder="ISBN" maxlength='50' required /></br>
			<input name="author" placeholder="Author" maxlength='40' required /></br>
			<input name="title" placeholder="Title" maxlength='40' required /><p>
			<input name="genre" placeholder="Genre" maxlength='40' required /><p>
			<input name="year" placeholder="Year" maxlength='40' required /><p>
			<textarea name="desc" required>Description </textarea><p>

			<input type="submit" name="submit" value="Add Book" />
		</form>
	</div>
</body>
</html>