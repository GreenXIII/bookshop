<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="s.css">
		<script type="text/javascript" src = "js/jquery.js"></script>
		<script type='text/javascript' src = 'js/js.js'></script>
		<meta charset="utf-8"/>
	</head>
<body>
<div class="wrapper">
<?php
	if(!isset($_GET['page']))
	{
		$page='home';
	}else{
		$page=$_GET['page'];
	}
	session_start();
	if(isset($_SESSION['user_login']) && isset($_SESSION['user_hash'])){
		switch ($page)
		{
			case 'home':
				include 'user_head.php'; 
				include 'book_list.php';
				break;
			case 'cart':
				require_once 'classes/users.php';
				$us = new Users();
				if($us->is_user($_SESSION['user_hash'], $_SESSION['user_login'], $_SESSION['user_type']))
				{	
					include 'cart.php'; 
				}
				break;
			case 'admin':
				require_once 'classes/users.php';
				$us = new Users();
				if($us->is_admin($_SESSION['user_hash'], $_SESSION['user_login'], $_SESSION['user_type']))
				{
					require_once 'classes/books.php';
					$book = new Book();
					require_once 'admin/add_book.php'; 
					require_once 'admin/bookslist.php'; 
				}
				break;
			case 'edit_book':
			require_once 'classes/users.php';
				$us = new Users();
				if($us->is_admin($_SESSION['user_hash'], $_SESSION['user_login'], $_SESSION['user_type']))
				{
					require_once 'classes/books.php';
					$book = new Book();
					require_once 'admin/edit_book.php';
				}
				break;
		}
	}else{
		switch ($page)
		{
			case 'enter':
				require_once 'enter.php';
				break;
			case 'signup':
				require_once 'signin.php';
				break;
			case 'home':
				echo "<div class='head'>";
					echo "<div class=right_head><a href='index.php?page=signup'>Sign In</a>";
					echo "<a  href='index.php?page=enter'>Log In</a></div>";
				echo "</div>";
				include 'book_list.php';
				break;
		}
	}
?>
</div>
</body>
</html>