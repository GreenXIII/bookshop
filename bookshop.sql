-- phpMyAdmin SQL Dump
-- version 3.3.0
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 08, 2014 at 09:58 AM
-- Server version: 5.1.44
-- PHP Version: 5.2.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `bookshop`
--

-- --------------------------------------------------------

--
-- Table structure for table `books`
--

CREATE TABLE IF NOT EXISTS `books` (
  `isbn` varchar(20) NOT NULL,
  `author` varchar(10) NOT NULL,
  `title` varchar(15) NOT NULL,
  `genre` varchar(20) NOT NULL,
  `year` int(4) NOT NULL,
  `descr` varchar(400) NOT NULL,
  PRIMARY KEY (`isbn`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `books`
--

INSERT INTO `books` (`isbn`, `author`, `title`, `genre`, `year`, `descr`) VALUES
('11111111111', '111', 'Big', '1', 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui'),
('111', '222', '333', '0', 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui'),
('33334', '3333', '3333', '0', 0, 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui'),
('9780091957155', 'Yotam Otto', 'Plenty More', 'General Cookery', 2014, 'Yotam Ottolenghis Plenty changed the way people cook and eat. Its focus on vegetable dishes, with the emphasis on flavour, original spicing and freshness of ingredients, caused a revolution not just in this country, but the world over. Plenty More picks up where Plenty left off, with 120 more dazzling vegetable-based dishes, this time organised by cooking method. Grilled, baked, simmered, cracked,'),
('9781472209405', 'Seth Caste', 'Underwater Pupp', '''Photography''', 2014, 'The world fell in love with swimming canines in Seth Casteels first book, Underwater Dogs. Now, in more than 80 previously unpublished portraits of underwater puppies, we see mans best friends at their most playful and exuberant. Each vibrant and colourful underwater image shows off the wild and sublime range of emotions of puppies, cute and irresistible to the very last.'),
('9781908005922', 'Ivy Press', 'Beautiful Owls ', '''Stationery''', 2014, 'The most arresting examples of the owl family feature in the 30 postcards contained within this book. A charmingly alternative set of portraits of these mysterious members of the bird world. The postcards are perforated at the spine for easy removal; send some flying off to family and friends, and keep your favorites for your own collection.'),
('9780008101268', 'LENA DUNHA', 'Not That Kind o', '''Humour''', 2014, 'Lena Dunham, acclaimed writer-director-star of HBO and Sky Atlantics Girls and the award-winning movie Tiny Furniture, displays her unique powers of observation, wisdom and humour in this exceptional collection of essays. If I could take what Ive learned and make one menial job easier for you, or prevent you from having the kind of sex where you feel you must keep your sneakers on in case you want'),
('9781780228228', 'Gillian Fl', 'Gone Girl', '''Crime''', 2014, 'THE ADDICTIVE No.1 BESTSELLER THAT EVERYONE IS TALKING ABOUT. Who are you? What have we done to each other? These are the questions Nick Dunne finds himself asking on the morning of his fifth wedding anniversary, when his wife Amy suddenly disappears. The police suspect Nick. Amys friends reveal that she was afraid of him, that she kept secrets from him. He swears it isnt true. A police examinatio'),
('9780007549795', 'SALI HUGHE', 'Pretty Honest', '''Cosmetics''', 2014, 'A witty, wise and truthful beauty handbook for real women on what works in real life from Sali Hughes, beloved journalist and broadcaster. Pour yourself a drink, put on some lipstick and pull yourself together Elizabeth Taylor Beauty books. Exquisite coffee-table affairs featuring improbably beautiful models with wholly-unachievable-to-most women looks, product review-heavy volumes which become al');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `login` varchar(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `type` varchar(2) NOT NULL,
  `hash` varchar(8) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `login`, `password`, `type`, `hash`) VALUES
(1, 'admin', '011c945f30ce2cbafc452f39840f025693339c42', 'ad', '12QHvTEC'),
(2, '111', '6216f8a75fd5bb3d5f22b6f9958cdede3fc086c2', 'us', 'O9kKK3dE');
